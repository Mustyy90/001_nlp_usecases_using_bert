ipython==7.13.0
matplotlib==3.2.1
pandas==1.0.3
xlrd==1.2.0
scikit-learn==0.22.2.post1
scipy==1.4.1

nltk==3.4.5
spacy==2.2.4
# run below with admin role
# python -m spacy download en
en-core-web-sm==2.2.5
Keras-Preprocessing==1.1.0
keras-bert==0.81.0

tensorboard==2.2.0 # TensorBoard is a suite of web applications for inspecting and understanding your TensorFlow runs and graphs.
tensorflow==2.2.0rc2 # TensorFlow is an open source software library for high performance numerical computation. Its flexible architecture allows easy deployment of computation across a variety of platforms (CPUs, GPUs, TPUs), and from desktops to clusters of servers to mobile and edge devices.
tensorflow-estimator==2.2.0rc0 # TensorFlow Estimator is a high-level API that encapsulates model training, evaluation, prediction, and exporting.
bert-tensorflow==1.0.1 # Official TensorFlow code and pre-trained models for BERT https://arxiv.org/abs/1810.04805 [maintained by Google]
tensorflow-hub==0.7.0 # TensorFlow Hub is a repository and library for reusable machine learning. The tfhub.dev repository provides many pre-trained models: text embeddings, image classification models, and more. The tensorflow_hub library lets you download and reuse them in your TensorFlow program with a minimum amount of code.

torch==1.4.0 # conda install pytorch torchvision cpuonly -c pytorch
transformers==2.6.0
pytorch-pretrained-bert==0.6.2

textblob==0.15.3

tweepy==3.8.0
wordcloud==1.6.0
# Markdown==3.2.1
# bokeh==1.3.4
# boto3==1.12.12
# bson==0.5.9
# exchangelib==3.1.1
# Flask==1.1.1
# html2text==2020.1.16
# pytz==2019.3
# PyYAML==5.3
# regex==2020.2.20
# requests==2.23.0