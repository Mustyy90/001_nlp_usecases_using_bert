from utils.data import get_pandas_df_from__001_file, get_text_and_subjectivity_tuple, clean_tweet

df = get_pandas_df_from__001_file()

tweets__series = df["TWEET_TEXT"]
tweets__series = [tweet for tweet in tweets__series]
tweets_list = set(tweets__series)

for tweet in tweets_list:
    _, subjectivity = get_text_and_subjectivity_tuple(clean_tweet(tweet).lower())
    if 0.0 < subjectivity < 0.1 or subjectivity > 0.9:
        print("Tweet:{}".format(tweet))
        print("\t\t\t\tSubjectivity:", subjectivity)
