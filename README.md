# 001_nlp_usecases_using_bert

* **Book Title**: NLP use-cases using BERT
* **Subtitle**: Transfer learning on pre-trained BERT models for NLP use-cases.
* **Target Audience**: Data Scientists, Machine Learning Engineers who are new to NLP and wanted to quickly implement different NLP use-cases. In the end, we would deep dive to develop production-ready Text Classification and NER model for their domain specific data. This book will cater examples in python 3.x and Scala 2.1x



# **Outline**:
1.  Introduction to NLP and different use-cases
2.	Deep Dive on Text Classification and different types of algorithms in industry
3.	Deep Dive on NER and different types of algorithms in industry
4.	Introduction of BERT and its application
5.	Concepts of Text Classification and NER models in BERT’s way
6.	Python Code Walk-through
    * Text Classification Model
    * NER Model
7.	Scala Code Walk-through 
    * Text Classification Model
    * NER Model

---